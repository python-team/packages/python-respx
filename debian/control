Source: python-respx
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders:
 Yogeswaran Umasankar <yogu@debian.org>,
Homepage: https://github.com/lundberg/respx
Vcs-Git: https://salsa.debian.org/python-team/packages/python-respx.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-respx
Section: python
Priority: optional
Build-Depends:
 debhelper-compat (= 13),
 dh-sequence-python3,
 python3-all,
 python3-anyio <!nocheck>,
 python3-flask <!nocheck>,
 python3-httpx,
 python3-pytest <!nocheck>,
 python3-pytest-asyncio <!nocheck>,
 python3-pytest-cov <!nocheck>,
 python3-pytest-tornasync <!nocheck>,
 python3-pytest-trio <!nocheck>,
 python3-pytest-twisted  <!nocheck>,
 python3-setuptools,
 python3-starlette <!nocheck>,
 python3-trio <!nocheck>,
 python3-wheel,
Build-Depends-Indep:
 mkdocs <!nodoc>,
 mkdocs-material <!nodoc>,
 python3-mkautodoc <!nodoc>,
Rules-Requires-Root: no
Standards-Version: 4.7.0
Testsuite: autopkgtest-pkg-pybuild

Package: python3-respx
Architecture: all
Depends:
 ${misc:Depends},
 ${python3:Depends},
Pre-Depends:
 ${misc:Pre-Depends},
Description: Utility for mocking out the Python HTTPX and HTTP Core libraries
 RESPX is a mock router, capturing requests sent by HTTPX,
 mocking their responses. Inspired by the flexible query API
 of the Django ORM, requests are filtered and matched against
 routes and their request patterns and lookups.

Package: python-respx-doc
Architecture: all
Section: doc
Depends:
 ${misc:Depends},
Multi-Arch: foreign
Description: Documentation for python-respx
 RESPX is a mock router, capturing requests sent by HTTPX,
 mocking their responses. Inspired by the flexible query API
 of the Django ORM, requests are filtered and matched against
 routes and their request patterns and lookups.
 .
 This package contains documentation for RESPX.
